# terraform-multi-environment

This project illustrates a common pattern in Terraform where each `environment` is defined in its
own separate directory. 

These directories may share modules with each other, but are otherwise independent. 

The GitLab integration is based on the GitLab-managed Terraform State and the
Terraform CI/CD Templates, see [the docs here](https://docs.gitlab.com/ee/user/infrastructure/iac/).

See [terraform-multi-environment-vars](https://gitlab.com/timofurrer/terraform-multi-environment-vars) for a similar 
approach where each `environment` is defined in its own `.tfvars` file.